<?php

/**
 * @file
 * Provides the form for imagecache_builder settings page.
 */

/**
 * Form builder function for module settings.
 *
 * @return array
 *   Settings form.
 */
function imagecache_builder_settings() {
  $filefield_form = imagecache_builder__get_filefield_form();
  if (isset($filefield_form) && !empty($filefield_form)) {
    $form['description'] = array(
      '#prefix' => '<div>',
      '#value' => t('The imagecache presets selected will be created for the relative image once that the respective node is created or updated.'),
      '#suffix' => '</div>',
    );
    $form[] = $filefield_form;
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
    // Call imagecache_builder_settings_submit() on form submission.
    $form['#submit'][] = 'imagecache_builder_settings_submit';
  }
  else {
    $form['warn'] = array(
      '#prefix' => '<div>',
      '#value' => t('You have to add at least an image field and an imagecache preset to get Imagecache Builder working.'),
      '#suffix' => '</div>',
    );
  }
  return $form;
}

/**
 * Get the elements with the imagefields.
 *
 * @return array
 *   Fields for setting form.
 */
function imagecache_builder__get_filefield_form() {
  // Get options saved.
  $settings = variable_get('imagecache_builder_settings', '');

  // Options to select.
  $options = array();
  $imagecache_presets = imagecache_presets();
  if (empty($imagecache_presets)) {
    return;
  }
  foreach ($imagecache_presets as $imagecache_preset) {
    $options[$imagecache_preset['presetid']] = $imagecache_preset['presetname'];
  }

  // Form element.
  $form = array();
  $info = _content_type_info();
  $info_content_types = $info['content types'];

  foreach ($info_content_types as $info_content_type) {
    $info_fields = $info_content_type['fields'];
    foreach ($info_fields as $info_field) {
      $images = array('imagefield', 'imagefield_crop');
      if (isset($info_field['widget']['module']) && in_array($info_field['widget']['module'], $images)) {
        $field_name = $info_field['field_name'];
        $key = 'icb_' . $info_content_type['type'] . '_' . $field_name;
        $default_value = (isset($settings[$key])) ? $settings[$key] : '';
        $form[$key] = array(
          '#title' => t('Field Image') . ' - ' . $info_field['field_name'] . ' (' . $info_content_type['type'] . ')',
          '#type' => 'select',
          '#description' => t('Choose the imagecache presets for this specific image field.'),
          '#default_value' => $default_value,
          '#options' => $options,
          '#multiple' => TRUE,
        );
      }
    }
  }
  return $form;
}

/**
 * Handle post-validation form submission.
 */
function imagecache_builder_settings_submit($form, &$form_state) {
  $result = array();
  foreach ($form_state['values'] as $key => $value) {
    if (strpos($key, 'icb_') !== FALSE) {
      $result[$key] = $value;
    }
  }
  // Set a persistent variable.
  variable_set('imagecache_builder_settings', $result);
  $message = t('Settings saved.');
  drupal_set_message($message);
}
