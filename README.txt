-- SUMMARY --
This module allows you to create all the imagecache presets you want
for a picture directly when its content is created or saved. Imagecache
creates a preset of an image only when that image is called, but sometimes
we need to call the pictures with different paths before that imagecache
has created them (for example using CDN).
This module solves the problem. This module uses an administration page
to set which imagecache presets create for the relative image fields.

-- FEATURES --
- Creates the presets when a node is saved or updated.
- Allows to selects which preset you want to create automatically
  for each image field.
- Prevents broken images not created yet by imagecache.

-- REQUIREMENTS --
* content (https://drupal.org/project/cck)
* filefield (https://drupal.org/project/filefield)
* imageapi, imageapi_gd (https://drupal.org/project/imageapi)
* imagecache, imagecache_ui (https://drupal.org/project/imagecache)
* imagefield (https://drupal.org/project/imagefield)

-- INSTALLATION --
* To install the module copy the 'imagecache_builder' folder to
   your sites/all/modules/contrib directory.
* Go to admin/build/modules. Enable the module.
   Read more about installing modules at http://drupal.org/node/70151
* Go to admin/build/imagecache/builder to configure the imagecache presets.
   You need to have already added at least an image field
   and an imagecache preset.
* When a content is saved or updated the module creates the imagecache presets
   chosen.

-- CONFIGURATION --
* Configure user permissions in Administration -> People -> Permissions.
* This module uses the permission 'administer imagecache'.

-- CONTACT --
Current maintainers:
* Gabriele Manna (geberele) - https://drupal.org/user/1183748
